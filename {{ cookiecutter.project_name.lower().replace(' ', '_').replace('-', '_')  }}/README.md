{{cookiecutter.project_name}}
==============================

{{cookiecutter.description}}

Getting Started
---------------

### Configure the conda environment

You can configure the packages you will need (you can add more later) in the 'environment.yml' file and create a conda environment

```shell
conda env create -f environment.yml
conda activate{{cookiecutter.project_name.lower().replace(' ', '_').replace('-', '_')}} 
```

*Note: It can take several minutes to solve the environment dependencies*

## Start a Jupyter notebook

```shell
cd notebooks
jupyter-lab
```

### Install the package
In the project root directory, run:
```shell
python setup.py develop
```
or
```shell
pip install -e .  
```

### Run the "tests"

```shell
py.test{{cookiecutter.project_name.lower().replace(' ', '_').replace('-', '_')}}   
```




Project Organization
--------------------

    ├── LICENSE
    ├── README.md          <- The top-level README for developers using this project.
    ├── Makefile           <- Makefile with commands like `make format` or `make check`,
    │                         additional conda package may have to be installed (e.g. flake8)
    ├── environment.yml    <- Conda environment file. Create environment with
    │                         `conda env create -f environment.yml`
    ├── setup.py           <- makes project pip installable (`pip install -e .`)
    |                         so source code can be imported    
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- A default Sphinx project; see README.md and sphinx-doc.org for details
    │   └── README.md      <- The original, immutable data dump.
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├──{{cookiecutter.project_name.lower().replace(' ', '_').replace('-', '_')}}        <- Source code for use in this project.
    │   ├── __init__.py    <- Makes project_name a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   └── make_dataset.py
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   └── build_features.py
    │   │
    │   ├── models         <- Scripts to train models and then to use
    │   │   │                 trained models to make predictions
    │   │   ├── predict_model.py
    │   │   └── train_model.py
    │   │         
    │   ├── tests          <- Functions to test the python modules
    │   │   └── test_dummy.py
    │   │   
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations
    │       └── visualize.py
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.readthedocs.io


Development
-----------

### Documentation

To build the documentation, open the [`docs/README.md`](docs/README.md)

### Code style

To format and check the code style, the following tools are used:

* [Black](https://github.com/psf/black)
* [blackdoc](https://github.com/keewis/blackdoc)
* [docformatter](https://github.com/myint/docformatter)
* [isort](https://pycqa.github.io/isort/)

Black and blackdoc loosely follows the [PEP8](http://pep8.org) guide but
with a few differences. Before committing, the formating can be run
automatically with:

```bash
make format
```

This project also uses [flake8](http://flake8.pycqa.org/en/latest/) and
[pylint](https://www.pylint.org/) to check the quality of the code and quickly catch
common errors.
The [`Makefile`](Makefile) contains rules for running both checks:

```bash
make check   # Runs black, blackdoc, docformatter, flake8 and isort (in check mode)
make lint    # Runs pylint, which is a bit slower
```

#### Docstrings

**All docstrings** should follow the
[numpy style guide](https://numpydoc.readthedocs.io/en/latest/format.html#docstring-standard).
All functions/classes/methods should have docstrings with a full description of all
arguments and return values.

The maximum line length for code is automatically set by *Black* and the docstrings
by *blackdoc*. To play nicely with Jupyter and IPython, **keep docstrings
limited to 79 characters** per line.

--------
--------
<p><small>Project based on the <a target="_blank" href="https://gitlab.com/public-lh/cookiecutter-project-template">cookiecutter science project template</a>.</small></p>
