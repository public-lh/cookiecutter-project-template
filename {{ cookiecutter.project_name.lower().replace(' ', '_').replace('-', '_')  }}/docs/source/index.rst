.. gcm-filters documentation master file, created by
   sphinx-quickstart on Tue Jan 12 09:24:23 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to {{cookiecutter.project_name.lower().replace(' ', '_').replace('-', '_')}} 's documentation!
=======================================

This is an awesome description of{{cookiecutter.project_name.lower().replace(' ', '_').replace('-', '_')}} , written in `reStructuredText`_.

You can contribute new sections by either adding a new RsT, Markdown file or jupyter notebook 
(see ``new_section.rst``, ``new_md_section.md``, ``new_notebook_section.md``).

In either case, make sure to add an entry in the ``.. toctree::``. Check out ``index.rst`` how it was done for the above examples.

.. toctree::
    :maxdepth: 3
    :glob:

    repo_readme
    new_section
    new_md_section
    new_notebook_section
    gallery
    api/{{cookiecutter.project_name.lower().replace(' ', '_').replace('-', '_')}}
    indices_and_tables



.. _reStructuredText: https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html
