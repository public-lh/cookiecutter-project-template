## Generate a local version of the documentation

### Set up the conda environment
Create and activate the Conda environment for the documentation *th_routing_docs*

```shell
conda env create -f environment.yml

conda activate {{cookiecutter.project_name.lower().replace(' ', '_').replace('-', '_')}}_docs
```

### Build the doc
```shell
make clean
```
#### html format
```shell
make html
```
#### PDF format
```shell
make latexpdf
```

To see all the different options run `make` in the `docs` folder
