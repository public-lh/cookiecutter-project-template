"""
Docstring for the dummy_module.py module.
"""


# Just a dummy function as example
def dummy_foo(a):
    """
    Dummy function for testing purpose

    Parameters
    ----------
    a: float
        a number

    Returns
    -------
    b: float
        the sum of a with 4

    Examples
    --------
    >>> from {{cookiecutter.project_name.lower().replace(' ', '_').replace('-', '_')}}.dummy_module import dummy_foo
    >>> test_num = dummy_foo(0)
    >>> print(test_num)
    4
    """

    return a + 4
